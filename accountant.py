import sys
from typing import Sized

ACTIONS = ("saldo", "zakup", "sprzedaz", "stop")# ----------------------------------------------------------tupla z akcjami
INPUT_ACTIONS = ("saldo", "sprzedaz", "zakup", "konto", "magazyn", "przeglad")# ----------------------tupla z akcjami z terminala
action = str()
account = int()
account = 10000000
warehouse = dict()
warehouse ={"a" : 10, "b" : 15, "c" : 20, "d" : 30 }
history = list()
input_list = sys.argv

if len(input_list) == 1:# --------------------------------------------------------------------- sprawdzenie poprawności uruchomienia programy
    print("=========== Błąd! ===========\n===== Uruchom program z akcją z listy: ('saldo', 'sprzedaz', 'zakup', 'konto', 'magazyn', 'przeglad') =====")
else:
    if input_list[1] not in INPUT_ACTIONS:# --------------------------------------------------- sprawdzenie poprawności uruchomienia programy
        print("=========== Błąd! ===========\n===== Akcja nie jest z listy =====")
    else:
        print("=========================== START ===========================")
        while not action == "stop":# ----------------------------------------------------------------- warunek "stop" przerwania programu
            choice = input(f"Wpisz akcję z dozwolonych {ACTIONS}: ")# --------------------------------------------------  część I zadania    
            if choice in ACTIONS:# ----------------------------------------------------------------- warunek przerwania programu
                action = choice
            else:# ----------------------------------------------------------------------------------- warunek przerwania programu
                print("======== Błąd! ========\n====== Zła akcja ======\n===== STOP! =====")
                break
            if action == "saldo":# ------------------------------------------------------------------------------akcja "saldo" z programu
                account_pay = int(input("Podaj kwotę wpłaty w groszach (jak wypłata to podaj kwotę ujemną): "))
                account += account_pay
                comment = input("Dodaj komentarz: ")
                if not comment:
                    comment = "- Nie dodano komentarza -"
                history.append([action, account_pay, comment])# ---------------------------------------------------------  część II zadania
            elif action == "zakup":# ----------------------------------------------------------------------------akcja "zakup" z programu
                while True:# ------------------------------------------------------------------------pętla do wprowadznia poprawnych wartości dla "zakup"
                    product = input("Podaj identyfikator kupowanego produktu: ")
                    price = input("Podaj cene jednostkową w groszach: ")# -------------------------------------------pętla sprawdzania czy
                    while not (price.isnumeric()):# -----------------------------------------------------------------podane znaki moga
                        price = input("! Podaj cene jednostkową w groszach: ")# -------------------------------------być liczbą dodatnią
                    price = int(price)# -----------------------------------------------------------------------------i zamienia na int()
                    if price >= 0:                
                        product_quantity = input("Podaj liczbę sztuk: ")# -------------------------------------------pętla sprawdzania czy
                        while not (product_quantity.isnumeric() and product_quantity != "0"):# ----------------------podane znaki moga
                            product_quantity = input("! Podaj liczbę sztuk: ")# -------------------------------------być liczbą dodatnią i != 0
                        product_quantity = int(product_quantity)# ---------------------------------------------------i zamienia na int()
                        if product_quantity > 0:
                            account-= product_quantity*price
                            if account < 0:
                                account += product_quantity*price
                                print("======== Błąd! ========\n===== Za mało gotówki =====")
                                break
                            else:
                                if product in warehouse:
                                    warehouse[product] += product_quantity
                                else:
                                    warehouse[product] = product_quantity
                        else:
                            print("======== Błąd! ========\n===== Zła ilość =====")
                            continue
                    else:
                        print("======== Błąd! ========\n===== Ujemna cena =====")
                        continue
                    break# -------------------------------------------------------------------------------------koniec pętli dla "zakup"
                history.append([action, product, price, product_quantity])#----------------------------------------------------  część II zadania
            elif action == "sprzedaz":# ------------------------------------------------------------------------akcja "sprzedaz" z programu
                if warehouse:# ----------------------------------------------------------------------------warunek sprawdza czy jest coś w magazynie
                    while True:# -----------------------------------------------------------------pętla do wprowadznia poprawnych wartości dla "sprzedaz"
                        print(f"Stan magazynu: {warehouse}")
                        product = input("Podaj identyfikator sprzedawanego produktu: ")
                        if not product in warehouse:
                            print("===== Błąd! =====\n===== Nie ma takiego towaru w magazynie =====")
                            continue
                        price = input("Podaj cene jednostkową w groszach: ")# -------------------------------------------pętla sprawdzania czy
                        while not (price.isnumeric()):# -----------------------------------------------------------------podane znaki moga
                            price = input("! Podaj cene jednostkową w groszach: ")# -------------------------------------być liczbą dodatnią
                        price = int(price)# -----------------------------------------------------------------------------i zamienia na int()                
                        if price >= 0:                    
                            product_quantity = input("Podaj liczbę sprzedawanych sztuk: ")# -----------------------------pętla sprawdzania czy
                            while not (product_quantity.isnumeric() and product_quantity != "0"):# ----------------------podane znaki moga
                                product_quantity = input("! Podaj liczbę sprzedawanych sztuk: ")# -----------------------być liczbą dodatnią i != 0
                            product_quantity = int(product_quantity)# ---------------------------------------------------i zamienia na int()
                            if product_quantity <= warehouse[product]:
                                account += product_quantity*price
                                warehouse[product] -= product_quantity
                                if warehouse[product] == 0:
                                    del warehouse[product]
                                history.append([action, product, price, product_quantity])# --------------------------------------  część II zadania
                                break
                            else:
                                print("======== Błąd! ========\n===== Zła ilość =====")
                                continue
                        else:
                            print("======== Błąd! ========\n===== Zła cena =====")
                            continue
                else:
                    print("======== Błąd! ========\n===== Magazyn jest pusty =====")
            else:# --------------------------------------------------------- tu action == "stop" ----------------------------------  część III zadania
                history.append([action])# ----------------------------------------------------------------------------------------  część II zadania
        while len(input_list) > 1:# ***************************************************************** od tego miejsca pobiera dane z listy z terminala
            if not choice in ACTIONS:
                break
            if input_list[1] == "saldo":# ----------------------------------------------------------------------------------------  część IV a) zadania
                if not len(input_list) == 4:
                    print("========== Błąd! ==========\n====== Zła ilość parametrów po akcji 'saldo' przy uruchamianiu programu ======")
                    break
                action = "saldo"
                account_pay = int(input_list[2])
                account += account_pay
                comment = input_list[3]
                del input_list[0]
                history.append(input_list)# ---------------------------------------------------------------------------------------  część II zadania
                print("\nWszystkie podane parametry w formie w jakiej zostały pobrane:")
                for list_in_history in history:# ------------------------------------------------------------------------------------ część V zadania
                    for item in list_in_history:
                        print(item)
                break
            elif input_list[1] == "sprzedaz":# ------------------------------------------------------------------------------------  część IV b) zadania
                if not len(input_list) == 5:
                    print("========== Błąd! ==========\n====== Zła ilość parametrów po akcji 'sprzedaz' przy uruchamianiu programu ======")
                    break
                action = "sprzedaz"
                product = input_list[2]
                product_quantity = int(input_list[4])
                if warehouse[product] >= product_quantity:
                    price = int(input_list[3])
                    if price >= 0:
                        if product_quantity > 0:
                            account += product_quantity*price
                            warehouse[product] -= product_quantity
                            if warehouse[product] == 0:
                                    del warehouse[product]
                        else:
                            print("======== Błąd! ========\n===== Zła ilość przy uruchomieniu programu =====")
                            break
                    else:
                        print("======== Błąd! ========\n===== Ujemna cena przy uruchomieniu programu =====")
                        break
                else:
                    print("======== Błąd! ========\n===== Brak Towaru. Zła ilość przy uruchamianiu programu =====")
                    break
                del input_list[0]
                history.append(input_list)# ----------------------------------------------------------------------------------------  część II zadania
                print("\nWszystkie podane parametry w formie w jakiej zostały pobrane:")
                for list_in_history in history:# ------------------------------------------------------------------------------------ część V zadania
                    for item in list_in_history:
                        print(item)                
                break
            elif input_list[1] == "zakup":# ----------------------------------------------------------------------------------------  część IV c) zadania
                if not len(input_list) == 5:
                    print("========== Błąd! ==========\n====== Zła ilość parametrów po akcji 'zakup' przy uruchamianiu programu ======")
                    break
                action = "zakup"
                product = input_list[2]
                price = int(input_list[3])
                if price >= 0:
                    product_quantity = int(input_list[4])
                    if product_quantity > 0:
                        account -= product_quantity*price
                        if account < 0:
                            account += product_quantity*price
                            print("======== Błąd! ========\n===== Brak gotówki na zakup towaru wpisanego przy uruchamianiu programu =====")
                            break
                        else:
                            if product in warehouse:
                                warehouse[product] += product_quantity
                            else:
                                warehouse[product] = product_quantity
                    else:
                        print("======== Błąd! ========\n===== Zła ilość przy uruchamianiu programu =====")
                        break
                else:
                    print("======== Błąd! ========\n===== Ujemna cena przy uruchamianiu programu =====")
                    break
                del input_list[0]
                history.append(input_list)# ------------------------------------------------------------------------------------------  część II zadania
                print("\nWszystkie podane parametry w formie w jakiej zostały pobrane:")
                for list_in_history in history:# -------------------------------------------------------------------------------------- część V zadania
                    for item in list_in_history:
                        print(item)                
                break
            elif input_list[1] == "konto":
                print(f"\nStan konta po wszystkich akcjach:")
                print(account)# ------------------------------------------------------------------------------------------------------  część IV d) zadania
                del input_list[0]
                history.append(input_list)# ------------------------------------------------------------------------------------------  część II zadania
                break
            elif input_list[1] == "magazyn":
                if not len(input_list) > 2 :
                    print("========== Błąd! ==========\n====== Zła ilość parametrów po akcji 'magazyn' przy uruchamianiu programu ======")
                    break
                del input_list[0]
                history.append(input_list)# -------------------------------------------------------------------------------------------  część II zadania
                if warehouse:         
                    print("\nStany magazynowe dla podanych produktów\nw formacie: <id produktu>: <stan>:")
                    for i in range (input_list.index(input_list[1]), len(input_list)):# -----------------------------------------------  część IV e) zadania
                        print(f"{input_list[i]}: {warehouse[input_list[i]]}")
                    break
                else:
                    print("====== Magazyn jest pusty ======")
                    break
            elif input_list[1] == "przeglad":
                if not len(input_list) == 4:
                    print("========== Błąd! ==========\n====== Zła ilość parametrów po akcji 'przegląd' przy uruchamianiu programu ======")
                    break
                del input_list[0]
                history.append(input_list)# -------------------------------------------------------------------------------------------  część II zadania
                if not len(history) >= int(input_list[2]):
                    print("===== Błąd! =====\n===== Podany zakres przekracza ilość zapisanych akcji ======")
                else:
                    print(f"\nAkcje zapisane pod indeksami w zakresie [{input_list[1]}, {input_list[2]}]:")
                    for list_in_history in range (int(input_list[1]), int(input_list[2]) + 1):#---------------------------------------------------------  część IV f) zadania
                        for list_item in history[list_in_history]:
                            print(list_item)
                print("\nWszystkie podane parametry w formie w jakiej zostały pobrane:")
                for list_in_history in history:# ------------------------------------------------------------------------------------ część V zadania
                    for item in list_in_history:
                        print(item)
                break
        print("=========================== STOP ===========================\n")
        print(f"Stan konta: {account} gr")
        print(f"Magazyn: {warehouse}")
        print(f"Historia: {history}")